import React, { Component } from "react";
import styles from "./Card.module.scss";
import Button from "../button/Button";
import { Icon } from "@iconify/react";
import PropTypes from "prop-types";

class Card extends Component {
  constructor(props) {
    super(props);
    this.state = {
      icon: "bi:star",
      inCart: false,
      inFavorite: false,
      product: this.props.product || {},
    };
  }

  componentDidMount() {
    const favoriteProduct = this.props.favoriteProducts.find(
      (product) => product.article === this.props.product.article
    );
    favoriteProduct &&
      this.setState({ inFavorite: true, icon: "bi:star-fill" });

    const cartProduct = this.props.cartProducts.find(
      (product) => product.article === this.props.product.article
    );
    cartProduct && this.setState({ inCart: true });
  }

  addToCart = () => {
    const cartProduct = this.props.cartProducts.find(
      (product) => product.article === this.state.product.article
    );
    if (cartProduct) return false;

    const newCartProducts = [...this.props.cartProducts, this.state.product];

    localStorage.setItem("Cart", JSON.stringify(newCartProducts));
    this.setState({ inCart: true });
    this.props.updateCart();
  };

  addToFavorite = () => {
    const favoriteProduct = this.props.favoriteProducts.find(
      (product) => product.article === this.state.product.article
    );
    if (favoriteProduct) return false;

    const newFavoriteProducts = [
      ...this.props.favoriteProducts,
      this.state.product,
    ];

    localStorage.setItem("Favorite", JSON.stringify(newFavoriteProducts));
    this.setState({ inFavorite: true });
    this.props.updateFavorite();
    this.setState({ icon: "bi:star-fill" });
  };

  removeFromFavorite = () => {
    const updatedFavoriteArray = this.props.favoriteProducts.filter(
      (product) => product.article !== this.state.product.article
    );
    localStorage.setItem("Favorite", JSON.stringify(updatedFavoriteArray));
    this.setState({ inFavorite: false });
    this.props.updateFavorite();
    this.setState({ icon: "bi:star" });
  };

  toggleFavorite = () => {
    this.state.inFavorite ? this.removeFromFavorite() : this.addToFavorite();
  };

  render() {
    return (
      <div className={styles.Card}>
        <div>
          <img
            alt="img"
            className={styles.CardImage}
            src={this.props.imgSrc}
          ></img>
        </div>
        <div className={styles.CardContent}>
          <h1 className={styles.CardTitle}>{this.props.title}</h1>
          <Icon onClick={this.toggleFavorite} icon={this.state.icon} />
          <p>{this.props.color}</p>
          <span className={styles.CardPrice}>${this.props.price}</span>
          <Button
            onClick={
              this.state.inCart
                ? null
                : () => {
                    this.props.openModal("toCartModal", this.addToCart);
                  }
            }
            text={this.state.inCart ? "IN CART." : "ADD TO CARD"}
            background="black"
          ></Button>
        </div>
      </div>
    );
  }
}

Card.propTypes = {
  product: PropTypes.object,
  updateCart: PropTypes.func,
  updateFavorite: PropTypes.func,
  openModal: PropTypes.func,
};

export default Card;
