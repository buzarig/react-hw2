const modalWindowDeclarations = [
  {
    modalId: "toCartModal",
    modalProps: {
      title: "Підтвердження",
      description: "Чи згодні ви додати цей товар у кошик?",
      background: "white",
      closeButton: true,
      actions: [
        {
          text: "Ні",
          background: "red",
          type: "cancel",
        },
        {
          text: "Так",
          background: "green",
          type: "submit",
        },
      ],
    },
  },
  {
    modalId: "modalID2",
    modalProps: {
      title: "title for modal 2",
      description: "description for modal 2",
      background: "yellow",
      closeButton: false,
      actions: [
        {
          text: "No",
          background: "red",
          type: "cancel",
        },
        {
          text: "Yes",
          background: "green",
          type: "submit",
        },
      ],
    },
  },
];

export default modalWindowDeclarations;
