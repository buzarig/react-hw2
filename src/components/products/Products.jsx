import React, { Component } from "react";
import Card from "../card/Card";
import styles from "./Products.module.scss";
import PropTypes from "prop-types";

class Products extends Component {
  render() {
    return (
      <div className={styles.ProductsList}>
        {this.props.products.map((product) => (
          <Card
            favoriteProducts={this.props.favoriteProducts}
            cartProducts={this.props.cartProducts}
            product={product}
            imgSrc={product.image}
            title={product.name}
            color={product.color}
            price={product.price}
            key={product.article}
            openModal={this.props.openModal}
            updateCart={this.props.updateCart}
            updateFavorite={this.props.updateFavorite}
          ></Card>
        ))}
      </div>
    );
  }
}

Products.propTypes = {
  products: PropTypes.array,
  imgSrc: PropTypes.string,
  title: PropTypes.string,
  color: PropTypes.string,
  price: PropTypes.number,
  openModal: PropTypes.func,
  updateCart: PropTypes.func,
  updateFavorite: PropTypes.func,
};

Products.defaultProps = {
  color: "black",
};

export default Products;
