import "./App.css";
import Products from "./components/products/Products";
import React, { Component } from "react";
import Modal from "./components/modal/Modal";
import Cart from "./components/cart/Cart";
import Favorite from "./components/favorite/Favorite";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      cartProducts: [],
      favoriteProducts: [],
      modal: {
        showModal: false,
        modalId: null,
        modalSubmit: null,
      },
    };
  }

  componentDidMount() {
    fetch("/products.json")
      .then((response) => response.json())
      .then((products) => {
        this.setState({ products });
      });

    localStorage.getItem("Cart")
      ? this.setState({
          cartProducts: JSON.parse(localStorage.getItem("Cart")),
        })
      : localStorage.setItem("Cart", JSON.stringify([]));

    localStorage.getItem("Favorite")
      ? this.setState({
          favoriteProducts: JSON.parse(localStorage.getItem("Favorite")),
        })
      : localStorage.setItem("Favorite", JSON.stringify([]));
  }

  updateCart = () => {
    this.setState({
      cartProducts: JSON.parse(localStorage.getItem("Cart")),
    });
  };

  updateFavorite = () => {
    this.setState({
      favoriteProducts: JSON.parse(localStorage.getItem("Favorite")),
    });
  };

  handleOpenModal = (modalId, modalSubmit) => {
    this.setState({
      modal: {
        showModal: true,
        modalId,
        modalSubmit,
      },
    });
  };

  handleCloseModal = () => {
    this.setState({
      modal: {
        showModal: false,
        modalId: null,
        modalSubmit: null,
      },
    });
  };

  render() {
    return (
      <>
        <header className="header">
          <Favorite counter={this.state.favoriteProducts.length} />
          <Cart counter={this.state.cartProducts.length} />
          {console.log(this.state.favoriteProducts)}
        </header>

        <Products
          products={this.state.products}
          openModal={this.handleOpenModal}
          updateFavorite={this.updateFavorite}
          updateCart={this.updateCart}
          favoriteProducts={this.state.favoriteProducts}
          cartProducts={this.state.cartProducts}
        />
        {this.state.modal.showModal && (
          <Modal
            close={this.handleCloseModal}
            modalId={this.state.modal.modalId}
            submit={this.state.modal.modalSubmit}
          />
        )}
      </>
    );
  }
}

export default App;
